class CoverUploader < CarrierWave::Uploader::Base

  include CarrierWave::MiniMagick

  storage :file

  process resize_to_fit: [1200, 800]

  version :medium do
    process resize_to_fill: [800,600]
  end

  version :thumb do
    process resize_to_fill: [400,400]
  end

  def extension_whitelist
    %w(jpg jpeg png)
  end

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

end
