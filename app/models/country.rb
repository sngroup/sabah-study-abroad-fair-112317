class Country < ApplicationRecord
  # Friendly URLs
  extend FriendlyId
  friendly_id :name, use: :slugged
  def should_generate_new_friendly_id?
    slug.blank? || name_changed?
  end

  # CarrierWave Related
  mount_uploader :cover, CoverUploader

  has_many :participants
end
