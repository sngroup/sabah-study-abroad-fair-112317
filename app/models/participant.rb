class Participant < ApplicationRecord
  belongs_to :country

  # CarrierWave Related
  mount_uploader :logo, LogoUploader
end
