class PagesController < ApplicationController

  layout "alternative", only: [:success]

  def index
    @countries = Country.order(name: :asc)
  end

  def attend
    registration_exists = 'Siz artıq qeydiyyatdan keçmisiniz!'
    registration_successful = 'Uqurla alindi'
    registration_unsuccessful = 'Zəhmət olmasa bir daha cəhd edin!'

    @fair_confirmation = FairConfirmation.new(fair_confirmation_params)

    if FairConfirmation.exists? email: @fair_confirmation[:email]
      redirect_to root_path, notice: registration_exists
      return
    end

    respond_to do |format|
      if @fair_confirmation.save
        format.html { redirect_to success_path }
        format.json { render :show, status: :created, location: @fair_confirmation }
      else
        format.html { redirect_to root_path, notice: registration_unsuccessful }
        format.json { render json: @fair_confirmation.errors, status: :unprocessable_entity }
      end
    end

  end

  def success
  end

  def registrations
    respond_to do |format|
      format.html
      format.csv { send_data FairConfirmation.all.to_csv }
    end
  end

  private
  
  def fair_confirmation_params
    params.require(:fair_confirmation).permit(:first_name, :last_name, :email, :phone, :source)
  end

end
