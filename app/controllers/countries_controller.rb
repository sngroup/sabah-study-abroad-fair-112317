class CountriesController < ApplicationController
  def show
    @country = Country.friendly.find(params[:country_id])
  end
end
