module ApplicationHelper

  # Meta tags
  def title(str)
    content_for(:title, content = str.to_s)
  end

  def description(str)
    content_for(:description, content = str.to_s)
  end

  def og_image(str)
    content_for(:og_image, content = str.to_s)
  end

  def classes(str)
    content_for(:classes, content = str.to_s)
  end

  def header(str)
    content_for(:header, content = str.to_s)
  end

  def summary(str)
    content_for(:summary, content = str.to_s)
  end

end
