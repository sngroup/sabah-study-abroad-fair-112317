countries = ActiveSupport::JSON.decode(File.read('db/data/countries.json'))
schools = ActiveSupport::JSON.decode(File.read('db/data/schools.json'))

countries.each do |country|
  Country.new(
    name: country['local_name'],
    short_name: country['short_name'],
    cover: Rails.root.join("db/images/#{country['cover_image']}").open,
    ).save(validate: false)
end

schools.each do |school|
  puts school
  Participant.new(
    name: school['name'],
    logo: Rails.root.join("db/images/#{school['logo']}").open,
    country_id: Country.find_by(:short_name => school['country_short_name']).id
    ).save(validate: false)
end