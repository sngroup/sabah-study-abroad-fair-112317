class CreateFairConfirmations < ActiveRecord::Migration[5.1]
  def change
    create_table :fair_confirmations do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :phone

      t.timestamps
    end
  end
end
