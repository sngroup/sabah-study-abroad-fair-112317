class AddSourceToFairConfirmations < ActiveRecord::Migration[5.1]
  def change
    add_column :fair_confirmations, :source, :string
  end
end
