Rails.application.routes.draw do

  root to: "pages#index"

  post '/attend', to: 'pages#attend'
  get '/success', to: 'pages#success'
  get '/bxkvfepycx', to: 'pages#registrations'

  get '/:country_id', to: 'countries#show', as: :country
end
